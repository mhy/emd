.. emd documentation master file, created by
   sphinx-quickstart on Sun Jan 27 23:11:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to emd's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials.rst
   reference.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
